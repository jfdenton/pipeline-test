#include <gtest/gtest.h>

#include "calculator.cpp"

TEST(AddTest, NormalCases) {
	ASSERT_EQ(3, add(1, 2));
	ASSERT_EQ(0, add(0, 0));
	ASSERT_EQ(225, add(125, 100));
}

TEST(AddTest, EdgeCases) {
	ASSERT_EQ(-2, add(0, -2));
	ASSERT_EQ(0, add(-2, 2));
}

int main(int argc, char** argv) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


